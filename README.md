# MyLovelySong

**French version**
Réaliser par Borel steve et Algassimou.
Ce projet consiste à permettre les passionnés de la musique de partager leurs chansons préferées avec d'autres utilisateurs.
Une fois que l'utilisateur accède sur l'application mobile:

- Il pourra consulter directement la liste les chansons partagées par d'autres


1. **Ecran d'acceuil**
</br>
 <p><img src="app/src/main/java/com/bs/mylovelysong/images/accueil.jpeg" alt="Acceuil" width="200" height="400"></p>
 </br>
- Ensuite s'il le souhaite, il pourra ajouter ses chansons préferées à lui.

2. **Ecran d'ajout**
</br>
<p><img src="app/src/main/java/com/bs/mylovelysong/images/addingpage.jpeg" alt="Acceuil" width="200" height="400"></P>

<p><img src="app/src/main/java/com/bs/mylovelysong/images/contact.jpeg" alt="Acceuil" width="200" height="400"></P>
</br>
Notre objectif est de régrouper les gens à travers la musique, au plaisir d'utiliser notre application.


**References**
- <p>https://www.chillcoding.com/android-retrofit-send-http/</p>
- <p>https://www.chillcoding.com/blog/2018/01/17/creer-bdd-sqlite-kotlin-android/</p>
- <p>https://developer.android.com/</p>
- <p>https://www.chillcoding.com/blog/2018/01/17/creer-bdd-sqlite-kotlin-android/</p>
- <p>https://www.chillcoding.com/blog/2014/10/10/utiliser-fichier-preferences/</p>
- <p>https://www.chillcoding.com/blog/2017/10/09/utiliser-bibliotheque-graphique-kotlin-android/</p>
- <p>https://www.chillcoding.com/blog/2018/10/22/creer-liste-recyclerview-kotlin-android/</p>


**English version**
realized by Algassimou & Baurel steve
This project is to allow music's lovers to share their favorite songs with other users.
Once the user accesses to the mobile application:

- He can directly consult the list of songs shared by others
- Then if he wishes, he can add his favorite songs too.

Our goal is to bring people together through music, looking forward to using our lovely app.


**Contact**
<p>[](https://www.linkedin.com/in/algassimou-barry-645107136/)</p>
<p>[](https://www.linkedin.com/in/ljbs1204/)</p>

