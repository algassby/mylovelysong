package com.bs.mylovelysong.model

data class Music(var id:Int?, var title:String, var album:String, var genre:String, var link:String, var likes:Int)
