package com.bs.mylovelysong.model

data class Singer(var id:Int, var name:String, var about:String)
