package com.bs.mylovelysong.model

data class Country(val name: String, val capital: String)
