package com.bs.mylovelysong

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bs.mylovelysong.adapter.SingersAdapter
import com.bs.mylovelysong.databinding.FragmentFirstBinding
import com.bs.mylovelysong.databinding.FragmentSecondBinding
import com.bs.mylovelysong.model.Singer
import com.bs.mylovelysong.service.SingerInterfaceRetrofit
import kotlinx.android.synthetic.main.fragment_first.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {

    var list = listOf<Singer>()

    private lateinit var binding :FragmentSecondBinding
            private val url = "http://10.0.2.2:8080/"
    val retrofit = Retrofit.Builder()
        .baseUrl(url)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
    val service = retrofit.create(SingerInterfaceRetrofit::class.java)
    val singerRequest = service.findAll()


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSecondBinding.inflate(layoutInflater)

        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        singerRequest.enqueue(object : Callback<List<Singer>> {
            override fun onResponse(call: Call<List<Singer>>, response: Response<List<Singer>>) {
                val allSinger = response.body()

                if (allSinger != null) {
                    Log.i("get All singers ", "SINGERS FROM LOVELY-SONG-API SERVER:")
                    for (singer in allSinger)
                        Log.i("one music is here"," one course : ${singer.name} : ${singer.about} ")
                }
                musicRecyclerView.adapter = allSinger?.toTypedArray()?.let { SingersAdapter(it) }
            }
            override fun onFailure(call: Call<List<Singer>>, t: Throwable) {
                error("KO")
            }
        })

        musicRecyclerView.layoutManager = LinearLayoutManager(context)
        binding.buttonSecond.setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
        }
    }
}