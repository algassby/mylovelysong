package com.bs.mylovelysong.dao

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class MusicDbHelper(ctx:Context): ManagedSQLiteOpenHelper(ctx,
    DB_NAME, null, DB_VERSION) {

    companion object {
        val DB_NAME = "music.db"
        val DB_VERSION = 4
    }
    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(
            MusicTable.NAME, true,
            MusicTable.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            MusicTable.TITLE to TEXT + NOT_NULL,
            MusicTable.ALBUM to TEXT,
            MusicTable.GENRE to TEXT,
            MusicTable.LINK to TEXT,
            MusicTable.LIKES to INTEGER + NOT_NULL
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(MusicTable.NAME, false)
        onCreate(db)
    }



}
