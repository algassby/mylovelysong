package com.bs.mylovelysong.dao

import com.bs.mylovelysong.model.Music
import com.bs.mylovelysong.request.MusicPOJO
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.db.update

class MusicDb (private val dbHelper: MusicDbHelper) {

    fun requestMusic() = dbHelper.use {
        select(MusicTable.NAME,
            MusicTable.ID, MusicTable.TITLE, MusicTable.ALBUM, MusicTable.GENRE, MusicTable.LINK, MusicTable.LIKES)
            .parseList(classParser<Music>())
    }

    fun saveMusic(music:  Music) = dbHelper.use {
        insert(MusicTable.NAME,
            MusicTable.TITLE to music.title,
            MusicTable.ALBUM to music.album,
            MusicTable.GENRE to music.genre,
            MusicTable.LINK  to music.link,
            MusicTable.LIKES to music.likes
            )
    }

    fun getOneMusci(id:Int) = dbHelper.use {
        select(MusicTable.NAME)
    }

    fun updateLike(id:Int, like:Int) = dbHelper.use{
        update(MusicTable.NAME, MusicTable.LIKES to (like + 1))
            .whereSimple(MusicTable.ID+" = ?", id.toString()).exec()
    }



    fun saveMusics(musicList: List<Music>) {
        for (music in musicList)
            saveMusic(music)
    }
}