
object MusicTable{
    val NAME  ="Music"
    val ID    = "_id";
    val TITLE = "title"
    val ALBUM = "album"
    val GENRE = "genre"
    val LINK  = "lien"
    val LIKES = "likes"
}