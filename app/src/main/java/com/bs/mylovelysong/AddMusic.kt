package com.bs.mylovelysong

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.bs.mylovelysong.dao.MusicDb
import com.bs.mylovelysong.dao.MusicDbHelper
import com.bs.mylovelysong.databinding.ActivityAddMusicBinding
import com.bs.mylovelysong.model.Music
import com.bs.mylovelysong.service.MusicService
import org.jetbrains.anko.doAsync
import splitties.activities.start
import splitties.alertdialog.*

class AddMusic : AppCompatActivity() {
    private lateinit var binding: ActivityAddMusicBinding
    private var music = Music(null,"","","", "", 0)
    val dbMusic by lazy { MusicDb(MusicDbHelper(applicationContext)) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddMusicBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.addMusic.setBackgroundColor(Color.GREEN)

        binding.previousBottom.setOnClickListener {
            start<MainActivity>()
        }

        binding.add.setOnClickListener{
                music.album = "United we are"
                music.title = "Spaceman"
                music.genre  =" Progressive house"
                music.link = "https://www.youtube.com/watch?v=xxMTgqNjZGo"
                music.likes = 0
            showAlertDialog()

        }
        Toast.makeText(this,R.string.success_add , Toast.LENGTH_LONG).show()


    }
    private fun add(){
        music.title = binding.title.text.toString()
        music.album = binding.album.text.toString()
        music.genre = binding.genre.text.toString()
        music.link  = binding.link.text.toString()
        music.likes = 0


        val song = StringBuilder()
            song
            .append("\n")
            .append(" title = ")
            .append(music.title)
            .append("\n album = ")
            .append(music.album)
            .append(" \n genre =")
            .append(music.genre).toString()
        Log.i("music = ",song.toString())
            if((music.title.isEmpty() || music.album.isEmpty() || music.genre.isEmpty())){
                Toast.makeText(this,R.string.failed_add , Toast.LENGTH_LONG).show()
                Log.i("ok","ok je suis dans le vide")
            }
        else{
                dbMusic.saveMusic(music)
                Log.i("ok","ok je sors du vide")
                start<MainActivity>()
            }

    }


    private fun showAlertDialog() {
        alertDialog {
            messageResource = R.string.text_alert
            okButton { add() }
            cancelButton { Log.i(MainActivity::class.simpleName, "Cancel") }

        }.onShow {
            positiveButton.setText(R.string.action_like)
        }.show()
    }
}