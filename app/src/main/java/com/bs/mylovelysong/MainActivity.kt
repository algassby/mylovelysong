package com.bs.mylovelysong

import MusicsAdapter
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bs.mylovelysong.dao.MusicDb
import com.bs.mylovelysong.dao.MusicDbHelper

import com.bs.mylovelysong.databinding.ActivityMainBinding
import com.bs.mylovelysong.model.Music
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread

import splitties.activities.start
import splitties.alertdialog.*
import java.lang.StringBuilder

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    //companion object for save how many times user lunch the mobile app
    companion object {
        var  NB = 0;
    }


    val courseDb by lazy { MusicDb(MusicDbHelper(applicationContext)) }
    var list = listOf<Music>()
    private val items = arrayOf(Music(null,"Jelly Bean", "ok", "ok","",0), Music(null,"Chris one", "best", "good","",0))
    var item = listOf<Music>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setSupportActionBar(binding.toolbar)

        binding.idMain.setBackgroundColor(Color.GREEN)


        binding.nextPageBtn.setOnClickListener {
            start<AddMusic>()
        }

        //contact us
        binding.contactButton.setOnClickListener {
            showAlertDialog()
        }
        NB++
        openRef()
    }

    /**
     * alert dialog for sending mail
     */
    private fun showAlertDialog() {
        alertDialog {
            messageResource = R.string.text_alert_contact
            okButton { sendEmail("lotchouang33@gmail.com", R.string.contactAboutApp.toString(), "Hello ...........!") }

            cancelButton { Log.i(MainActivity::class.simpleName, "Cancel") }


        }.onShow {
            positiveButton.setText(R.string.action_like)
        }.show()
    }
    /**
     * save the num when we out in mobile app
     */
    override fun onDestroy() {
        super.onDestroy()
        var defPref = PreferenceManager.getDefaultSharedPreferences(this)
        Log.i(MainActivity::class.simpleName, "nb is ${defPref.getInt(App.PREF_NB.toString(), NB)}")
    }
    fun openRef(){
        var devPref =  PreferenceManager.getDefaultSharedPreferences(this)
        var editor = devPref.edit()
        editor.putInt(App.PREF_NB, NB)
        editor.commit()
    }
    private fun showList() {
        Log.i("NB MUSIC :", list.size.toString())
        for (m in list)
            Log.i("Voici une chanson :", StringBuilder().append(m.title).append(", ")
                .append(m.album)
                .append(", ")
                .append(m.genre).toString())
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_contact -> true

            else -> super.onOptionsItemSelected(item)
        }
    }

    //sendMail
    private fun sendEmail(to: String, subject: String, msg: String) {
        val emailIntent = Intent(Intent.ACTION_SEND)

        emailIntent.data = Uri.parse("mailto:")
        emailIntent.type = "text/plain"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        emailIntent.putExtra(Intent.EXTRA_TEXT, msg)

        try {
            startActivity(Intent.createChooser(emailIntent, getString(R.string.title_send_email)))
        } catch (ex: ActivityNotFoundException) {
            toast(R.string.text_no_email_client)
        }
    }
}