package com.bs.mylovelysong.service

import com.bs.mylovelysong.model.Music
import com.bs.mylovelysong.model.Singer
import retrofit2.Call
import retrofit2.http.*

interface SingerInterfaceRetrofit {
    @GET("/api/singers")
    fun findAll():Call<List<Singer>>

}