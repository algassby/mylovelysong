import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bs.mylovelysong.dao.MusicDb
import com.bs.mylovelysong.dao.MusicDbHelper
import com.bs.mylovelysong.databinding.MusicItemBinding
import com.bs.mylovelysong.model.Music
import kotlinx.android.synthetic.main.music_item.view.*
import okhttp3.internal.notify


class MusicsAdapter(val items: Array<Music>) : RecyclerView.Adapter<MusicsAdapter.ViewHolder>(){

    class ViewHolder(val binding: MusicItemBinding, context:Context) : RecyclerView.ViewHolder(binding.root){
        private val context: Context = context
        val musicDb by lazy { MusicDb(MusicDbHelper(context)) }
        fun bindMusic(music: Music) {
            with(music) {
                binding.musicNameId.text = title
                binding.albumId.text = album
                binding.genreId.text = genre
                binding.ecouterId.text = "ecouter"
                binding.likeId.text = likes.toString()
                binding.ecouterId.setOnClickListener {
                    val url = link
                    val i =   Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                        context.startActivity(i)
                }
                binding.favBtn.setOnClickListener {
                    id?.let {
                            it1 -> musicDb.updateLike(it1,likes)
                    }
                }
            }
        }

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = MusicItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding,parent.context)

    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindMusic(items[position])
    }
    override fun getItemCount(): Int {
        return items.size
    }
}
