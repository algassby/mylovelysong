package com.bs.mylovelysong.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import com.bs.mylovelysong.databinding.SingerItemBinding

import com.bs.mylovelysong.model.Singer

class SingersAdapter (val items: Array<Singer>) : RecyclerView.Adapter<SingersAdapter.ViewHolder>() {
    class ViewHolder(val binding: SingerItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindSinger(singer: Singer) {
            with(singer) {
                binding.singerNameId.text = name
                binding.aboutId.text = about
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = SingerItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindSinger(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }
}