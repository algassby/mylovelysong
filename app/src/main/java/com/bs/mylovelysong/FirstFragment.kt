package com.bs.mylovelysong

import MusicsAdapter
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bs.mylovelysong.dao.MusicDb
import com.bs.mylovelysong.dao.MusicDbHelper
import com.bs.mylovelysong.databinding.FragmentFirstBinding
import com.bs.mylovelysong.model.Music
import com.bs.mylovelysong.service.MusicService

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_first.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.StringBuilder

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {
    private lateinit var binding : FragmentFirstBinding
    val musicDb by lazy { MusicDb(MusicDbHelper(requireContext())) }
    var list = listOf<Music>()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFirstBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //change the color of background
        binding.musicListId.setBackgroundColor(Color.rgb(0,255,0))
        musicRecyclerView.layoutManager = LinearLayoutManager(context)



        binding.buttonFirst.setOnClickListener {
            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
        }

        list = musicDb.requestMusic()
        musicRecyclerView.adapter = MusicsAdapter(list.toTypedArray())

    }

    private fun showList() {
        Log.i("NB MUSIC :", list.size.toString())
        for (m in list)
            Log.i("Voici une chanson :", StringBuilder().append(m.title).append(", ")
                .append(m.album)
                .append(", ")
                .append(m.genre).toString())
    }

    override fun onResume() {
        super.onResume()
        binding.musicRecyclerView.adapter?.notifyDataSetChanged()
    }
}